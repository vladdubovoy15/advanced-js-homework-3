"use strict";

class Employee {
    constructor(name, age, salary) {
        this.name = name;
        this.age = age;
        this.salary = salary;
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this.lang = lang;
    }

    get calcSalary(){
        return this.salary * 3;
    }
}

let user1 = new Programmer("Vlad", 26, 10000, ["russian", "english", "ukrainian"]);
let user2 = new Programmer("Olga", 28, 30000, ["Ukrainian", "english", "spanish"]);

console.log(user1);
console.log(user2);